Simple Flask static site with cats. You can run it from command line by 
```
python app.py
```

Project requires python 3 and Flask library.
